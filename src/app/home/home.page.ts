import { Component } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { MyModalPage } from '../modals/my-modal/my-modal.page'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private modalCtrl:ModalController,
    private alertCtrl:AlertController
  ) {}


  async moveToFirst()
  {
    const modal = await this.modalCtrl.create({
     component: MyModalPage,
     cssClass: "wideModal"
   });

   return await modal.present();
  }
}


